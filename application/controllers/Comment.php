<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Comment extends CI_Controller {

   public $comment;

   public function __construct() {
      parent::__construct(); 
      if(!$this->session->userdata('id'))
      {
        redirect('login');
      }
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->model('comments_model');
      $this->comment = new Comments_Model;
      $this->load->model('register_model');
   }

   public function index()
   {
       $data['data'] = $this->comment->get_comment();  
       $this->load->view('list',$data);
   }

   public function show($id)
   {
      $comment = $this->comment->find_item($id);
      $this->load->view('show',array('comment'=>$comment));
   }

   public function create()
   {
      $this->load->view('comment');
   }

   public function add_comment()
   {
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('comment'));
        }else{
             $result = $this->comment->insert_comment();
             if($result){
              $this->register_model->user_log($this->session->userdata('id'),'Created');
             }
             $this->session->set_flashdata('message', 'Comment Successfully Added!');
             redirect(base_url('comment'));
        }
    }

   public function edit($id)
   {
       $comment = $this->comment->find_item($id);

       $this->load->view('edit',array('comment'=>$comment));
   }

   public function update($id)
   {
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('edit/'.$id));
        }else{ 
          $result=$this->comment->update_item($id);
          if($result){
              $this->register_model->user_log($this->session->userdata('id'),'Updated');
              $this->session->set_flashdata('message', 'Comment Successfully Updated!');
             }
          redirect(base_url('comment'));
        }
   }

   public function delete($id)
   {
       $comment = $this->comment->delete_item($id);
       $this->register_model->user_log($this->session->userdata('id'),'Deleted');
       $this->session->set_flashdata('message', 'Comment Successfully Deleted!');
       redirect(base_url('comment'));
   }
}