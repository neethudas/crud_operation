<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

      public function __construct()
      {
          parent::__construct();
          if($this->session->userdata('id'))
          {
            redirect('private_area');
          }
            $this->load->library('form_validation');
            //$this->load->library('encrypt');
            $this->load->model('login_model');
            $this->load->model('register_model');
      }
      function index()
      {
          $this->load->view('login');
      }
      function validation()
      {
          if($this->input->post('user_email') && $this->input->post('user_password'))
          {
              $result = $this->login_model->can_login($this->input->post('user_email'), $this->input->post('user_password'));
              if($result == '1')
              {   
                  $this->register_model->user_log($this->session->userdata('id'),'Login');
                  $this->session->set_flashdata('message',$result);
                  redirect('comment');
              }
              else
              {            
                  $this->session->set_flashdata('message',$result);
                  redirect('login');
              }
          }
          else
          {
              $this->index();
          }
      }

}

?>