<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Private_area extends CI_Controller {
public function __construct()
{
    parent::__construct();
    if(!$this->session->userdata('id'))
    {
        redirect('login');
    }
        $this->load->model('register_model');
        $this->load->model('comments_model');

    }

    function index()
    {
        $data = $this->comments_model->get_comment();
        $this->load->view('list',array('data'=>$data));
    }

    function logout()
    {
        $this->register_model->user_log($this->session->userdata('id'),'Logout');
        $data = $this->session->all_userdata();
        foreach($data as $row => $rows_value)
        {
        $this->session->unset_userdata($row);
        }
        redirect('login');
    }
}

?>
