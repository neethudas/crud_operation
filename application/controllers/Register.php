<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('id'))
        {
          redirect('private_area');
        }
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->model('register_model');
    }
    public function index()
    {
        $this->load->view('register');
    }

    public function validation()
    {
      $this->load->library('encryption');
// echo '<pre>';
//       print_r($this->input->post());exit;
        // $this->form_validation->set_rules('user_name', 'Name', 'required|trim');
        // $this->form_validation->set_rules('user_email', 'Email Address', 'required|trim|valid_email|is_unique[user.user_email]');
        // $this->form_validation->set_rules('user_password', 'Password', 'required');
        if($this->input->post())
        {
            $encrypted_password = md5($this->input->post('user_password'));
            $verification_key = md5(rand());
            $data = array(
            'first_name'  => $this->input->post('first_name'),
            'last_name' =>  $this->input->post('last_name'),
            'user_email'  => $this->input->post('user_email'),
            'user_password' => $encrypted_password,
            'verification_key' => $verification_key
            );
            $result = $this->register_model->insert($data);
            if($result){
              $this->session->set_flashdata('message', 'You are successfully registered!');
              redirect('login');
            }
        }
    }
    function activity()
    {
      $this->load->view('activity');
    }

    }

    ?>