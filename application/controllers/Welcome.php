<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('comment');
	}
	public function activity_logs(){
		$this->db->where('notify',1);
		$this->db->select('user.first_name,app_login_data.*');
		$this->db->join('user','user.id = app_login_data.user_id', 'left');
		$this->db->order_by('created_at',"DESC");
		$this->db->limit(10); 
	    $query = $this->db->get('app_login_data');
	    $activity_array=[];
	    foreach ($query->result() as $key => $activities) {
	    	$activity_array[$key]['id']= $activities->id;
	    	$activity_array[$key]['user_id']= $activities->user_id;
	    	$activity_array[$key]['created_at']= $activities->created_at;
	    	$activity_array[$key]['action']= $activities->action;
	    	$activity_array[$key]['first_name']= $activities->first_name;
	    }
	    echo json_encode($activity_array);
	}
}
