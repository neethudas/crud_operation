<?php


class Comments_Model extends CI_Model{

// function for list comments
    public function get_comment(){
        $query = $this->db->get("comments");
        return $query->result();
    }
// function for update comments
    public function update_item($id) 
    {
        $data=array(
            'name' => $this->input->post('name'),
            'comment'=> $this->input->post('comment')
        );
        if($id==0){
            return $this->db->insert('comments',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('comments',$data);
        }        
    }
    public function insert_comment()
    {    
        $data = array(
            'name' => $this->input->post('name'),
            'comment' => $this->input->post('comment'),
            'user_id' => $this->session->userdata('id'),
            'created_by' => $this->session->userdata('id'),
        );
        return $this->db->insert('comments', $data);
    }

// function for find comments
    public function find_item($id)
    {
        return $this->db->get_where('comments', array('id' => $id))->row();
    }

// function for delete comments
    public function delete_item($id)
    {
        return $this->db->delete('comments', array('id' => $id));
    }
}



?>