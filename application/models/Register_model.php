<?php
class Register_model extends CI_Model
{
  function insert($data)
  {
    $this->db->insert('user', $data);
    return $this->db->insert_id();
  }

  function verify_email($key)
  {
    $this->db->where('verification_key', $key);
    $this->db->where('is_email_verified', 'no');
    $query = $this->db->get('user');
    if($query->num_rows() > 0){
      $data = array(
      'is_email_verified'  => 'yes'
      );
      $this->db->where('verification_key', $key);
      $this->db->update('user', $data);
      return true;
      } else {
        return false;
      }
  }
  public function user_log($user_id , $action){

    $log_data = [ 'user_id' => $user_id,
                  'action' => $action,
                  'notify' => 1 ];
    $this->db->insert('app_login_data', $log_data);

  }

}

?>