<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="utf-8">
    <title>Activity Log</title>
</head>
<body>
    <div class="container">
        <div align="center"><a href="<?php echo base_url(); ?>login" target= "_blank">Login/Register</a></div>

        <div class="cw-info-box">
            <p><strong>Visitor Activity Log</strong></p>
            <div class="log-data" id="log">
                <p><?php// echo $user_name.' '.$action .'at'. $time;?></p>
            </div>
        </div>
    </div>
    <div>
    </div>
    <script type="text/javascript">

        setInterval("yourActivity()",3000);
        function yourActivity(){
        $.ajax({
            url : "<?php echo base_url();?>welcome/activity_logs",
            type: "POST",
            success: function(data, textStatus, jqXHR)
            {
                console.log(data);
                var options ='';
                var result = $.parseJSON( data );
                $.each(result, function( index, value ) {
                    options += "<span>"+value.first_name+"  "+value.action+" at: "+value.created_at+"</span><br>";
                    $('#log').html(options);
                }); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
         
            }
        });
        }

</script>
</body>
</html>

