<!DOCTYPE html>
<html>
<head>
 <title>Leave a comment</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
 <div align="center"><a href="<?php echo base_url(); ?>private_area/logout">Logout</a></div>
</head>
<body>
 <div class="container main_class">
  <div class="pull-right">
    <a class="btn btn-success" href="<?php echo base_url('comment/index') ?>"> Create New Comment</a>
  </div>
  <h3 align="center">List comment</h3>
  <br />
  <div class="panel panel-default">
   <div class="panel-body">
    <form method="post" action="<?php echo base_url(); ?>comment/add_comment">
      <div class="form-group">
      <label>Enter Your Name</label>
      <input type="text" name="name" class="form-control" value="<?php echo set_value('name'); ?>" />
      <span class="text-danger"><?php echo form_error('name'); ?></span>
     </div>
     <div class="form-group">
      <label>Enter Your Comment</label>
      <input type="textarea" name="comment" class="form-control" value="<?php echo set_value('comment'); ?>" />
      <span class="text-danger"><?php echo form_error('comment'); ?></span>
     </div>
     <div class="form-group">
      <input type="submit" name="Submit" value="Submit" class="btn btn-info" />
     </div>
    </form>
   </div>
  </div>
 </div>
</body>
</html>


<style type="text/css">
  .main_class {
    width: 1000px;
      background-color: #aecad3;
      margin-top: 20px;
</style>
