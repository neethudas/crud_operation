
<!DOCTYPE html>
<html>
<head>
    <title>List Comments</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <div align="center"><a href="<?php echo base_url(); ?>private_area/logout">Logout</a></div>
</head>
<body>
<div class="container list_class">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>List Comments</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="<?php echo base_url('comment/create') ?>"> Create New Comment</a>
        </div>
    </div>
</div>
<table class="table table-bordered">
  <thead>
      <tr>
          <th>id</th>
          <th>Name</th>
          <th>Comments</th>
          <th width="220px">Action</th>
      </tr>
  </thead>
  <tbody>
   <?php 
   $i = 1;
   foreach ($data as $comments) { ?>      
      <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $comments->name; ?></td>
          <td><?php echo $comments->comment; ?></td>          
          <td>
            <form method="DELETE" action="<?php echo base_url('comment/delete/'.$comments->id);?>">
            <a class="btn btn-info" href="<?php echo base_url('comment/show/'.$comments->id) ?>"> show</a>
            <a class="btn btn-primary" href="<?php echo base_url('comment/edit/'.$comments->id) ?>"> Edit</a>
            <button type="submit" class="btn btn-danger"> Delete</button>
            </form>
          </td>     
      </tr>

      <?php 
    $i ++;
  } ?>
  </tbody>
</table>
</div>
 </body>
</html>
<style>
.list_class{
  
    width: 1170px;
    margin-top: 30px;
    background-color: #c9d7da;
}
</style>
