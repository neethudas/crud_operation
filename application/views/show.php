
<!DOCTYPE html>
<html>
<head>
    <title>Comments</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <div align="center"><a href="<?php echo base_url(); ?>private_area/logout">Logout</a></div>
</head>
<body>
<div class="container show_class">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Comment</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="<?php echo base_url('comment');?>"> Back</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            <?php echo $comment->name; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Comment:</strong>
            <?php echo $comment->comment; ?>
        </div>
    </div>
</div>
</div>
 </body>
</html>
<style>
.show_class{
  
    width: 1170px;
    margin-top: 30px;
    background-color: #c9d7da;
}
</style>